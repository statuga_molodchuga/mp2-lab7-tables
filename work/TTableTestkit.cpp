#include "gtest/gtest.h"

#include "Students.h"
#include "TSortTable.h"
#include "TTreeTable.h"
#include "TBalanceTree.h"
#include "TArrayHash.h"
#include "TListHash.h"


//TABLE RECORD TEST

TEST(TTabRecordTest, can_create_default_TTabRecord)
{
	ASSERT_NO_THROW(TTabRecord());
}

TEST(TTabRecordTest, can_create_TTabRecord)
{
	ASSERT_NO_THROW(TTabRecord("ABC", nullptr));
}

TEST(TTabRecordTest, can_get_key)
{
	Students* data = new Students();
	data->AddCource("Philosphy", 5);
	TTabRecord rec("ABC", data);

	EXPECT_EQ("ABC", rec.GetKey());
}

TEST(TTabRecordTest, can_set_key)
{
	Students* data = new Students();
	data->AddCource("Philosphy", 5);
	TTabRecord rec("ABC", data);

	rec.SetKey("BCA");

	EXPECT_EQ("BCA", rec.GetKey());
}


TEST(TTabRecordTest, op_equal_TTR)
{
	Students* data1 = new Students();
	Students* data2 = new Students();

	data1->AddCource("History", 4);
	data1->AddCource("Philosphy", 5);

	data2->AddCource("AVS", 3);
	data2->AddCource("OS", 2);

	TTabRecord tr1("ABC", data1);
	TTabRecord tr2("BCA", data2);

	ASSERT_NO_THROW(tr1 = tr2);
	EXPECT_TRUE(tr1.GetKey() == tr2.GetKey());
	EXPECT_TRUE(*(Students*)tr1.GetValuePTR() == *(Students*)tr2.GetValuePTR());
}

//SCAN TABLE TEST


TEST(TScanTableTest, can_get_tab_size)
{
	Students* data1 = new Students();
	Students* data2 = new Students();

	data1->AddCource("History", 4);
	data1->AddCource("Philosphy", 5);

	data2->AddCource("AVS", 5);
	data2->AddCource("OS", 4);

	TScanTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);

	EXPECT_EQ(table1.GetTabSize(), 5);
}

TEST(TScanTableTest, is_full_return_true_if_table_is_full)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(3);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Palmer Lora", data3);

	EXPECT_TRUE(table1.IsFull());
}

TEST(TScanTableTest, is_full_return_false_if_table_is_not_full)
{
	Students* data3 = new Students();

	data3->AddCource("AVS", 5);

	TScanTable table1(3);
	table1.InsRecord("Palmer Lora", data3);

	EXPECT_FALSE(table1.IsFull());
}

TEST(TScanTableTest, is_empty_return_true_if_table_is_empty)
{
	Students* data1 = new Students();

	data1->AddCource("History", 4);

	TScanTable table1(3);
	table1.InsRecord("Palmer Lora", data1);
	table1.DelRecord("Palmer Lora");
	EXPECT_TRUE(table1.IsEmpty());
}

TEST(TScanTableTest, is_empty_return_false_if_table_is_not_empty)
{
	Students* data3 = new Students();

	data3->AddCource("AVS", 5);

	TScanTable table1(3);
	table1.InsRecord("Palmer Lora", data3);

	EXPECT_FALSE(table1.IsEmpty());
}


TEST(TScanTableTest, can_get_value_from_current_position)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Newman Carol", data3);
	table1.SetCurrentPos(1);

	EXPECT_EQ(*(Students*)table1.GetValuePTR(CURRENT_POS), *data2);
}

TEST(TScanTableTest, can_get_value_from_last_position)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Newman Carol", data3);
	table1.SetCurrentPos(1);

	EXPECT_EQ(*(Students*)table1.GetValuePTR(LAST_POS), *data3);
}


TEST(TScanTableTest, can_reset_scan_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Newman Carol", data3);
	table1.Reset();

	EXPECT_EQ(table1.GetKey(CURRENT_POS), "Briggs Holly");
}

TEST(TScanTableTest, can_go_next_scan_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Newman Carol", data3);
	table1.Reset();
	table1.GoNext();

	EXPECT_EQ(table1.GetKey(CURRENT_POS), "May Nancy");
}


TEST(TScanTableTest, cant_delete_record_from_scan_table_if_this_record_does_not_exist)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);

	ASSERT_ANY_THROW(table1.DelRecord("Newman Carol"));
}

TEST(TScanTableTest, can_insert_record_in_scan_table)
{
	Students* data2 = new Students();

	data2->AddCource("Philosphy", 5);

	TScanTable table1(5);
	table1.InsRecord("May Nancy", data2);

	EXPECT_EQ(table1.GetKey(FIRST_POS), "May Nancy");
}

TEST(TScanTableTest, cant_insert_record_in_full_scan_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable table1(3);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Newman Carol", data3);
	ASSERT_ANY_THROW(table1.InsRecord("Conley Hilda", data3));
}

TEST(TScanTableTest, cant_set_incorrect_current_pos_in_scan_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);

	TScanTable table1(3);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);

	ASSERT_ANY_THROW(table1.SetCurrentPos(5));
}

TEST(TScanTableTest, cant_set_negative_current_pos_in_scan_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);

	TScanTable table1(3);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);

	ASSERT_ANY_THROW(table1.SetCurrentPos(-5));
}

//SORT TABLE TEST



TEST(TSortTableTest, cant_delete_record_from_sorted_table_if_this_record_does_not_exist)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TSortTable table1(5);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);

	ASSERT_ANY_THROW(table1.DelRecord("Newman Carol"));
}

TEST(TSortTableTest, can_insert_record_in_sort_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TSortTable table1(3);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("Underwood Joanna", data3);

	EXPECT_EQ(table1.GetKey(FIRST_POS), "Briggs Holly");
	EXPECT_EQ(table1.GetKey(LAST_POS), "Underwood Joanna");
}

TEST(TSortTableTest, cant_insert_record_in_full_sortes_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TSortTable table1(3);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("May Nancy", data2);
	table1.InsRecord("Newman Carol", data3);
	ASSERT_ANY_THROW(table1.InsRecord("Conley Hilda", new Students(*data3)));
}

TEST(TSortTableTest, cant_insert_record_in_sorted_table_if_this_record_exists)
{
	Students* data1 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data3->AddCource("AVS", 5);

	TSortTable table1(3);
	table1.InsRecord("Briggs Holly", data1);
	table1.InsRecord("Newman Carol", data3);

	ASSERT_ANY_THROW(table1.InsRecord("Newman Carol", data3));
}

TEST(TSortTableTest, no_throw_when_sorted_table_equale_scan_table)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable ScanT(3);
	ScanT.InsRecord("May Nancy", data2);
	ScanT.InsRecord("Newman Carol", data3);
	ScanT.InsRecord("Briggs Holly", data1);

	TSortTable SortT(ScanT);
	ASSERT_NO_THROW(SortT = ScanT);
}

TEST(TSortTableTest, when_sorted_table_equale_scan_table_item_have_correct_position_during_insertion_sort)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable ScanT(3);
	ScanT.InsRecord("May Nancy", data2);
	ScanT.InsRecord("Underwood Joanna", data3);
	ScanT.InsRecord("Briggs Holly", data1);

	TSortTable SortT(3);
	SortT.SetSortMethod(INSERT_SORT);
	SortT = ScanT;

	EXPECT_EQ(SortT.GetKey(FIRST_POS), "Briggs Holly");
	EXPECT_EQ(SortT.GetKey(LAST_POS), "Underwood Joanna");
}

TEST(TSortTableTest, when_sorted_table_equale_scan_table_item_have_correct_position_during_merge_sort)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable ScanT(3);
	ScanT.InsRecord("May Nancy", data2);
	ScanT.InsRecord("Underwood Joanna", data3);
	ScanT.InsRecord("Briggs Holly", data1);

	TSortTable SortT(3);
	SortT.SetSortMethod(MERGE_SORT);
	SortT = ScanT;

	EXPECT_EQ(SortT.GetKey(FIRST_POS), "Briggs Holly");
	EXPECT_EQ(SortT.GetKey(LAST_POS), "Underwood Joanna");
}

TEST(TSortTableTest, when_sorted_table_equale_scan_table_item_have_correct_position_during_quick_sort)
{
	Students* data1 = new Students();
	Students* data2 = new Students();
	Students* data3 = new Students();

	data1->AddCource("History", 4);
	data2->AddCource("Philosphy", 5);
	data3->AddCource("AVS", 5);

	TScanTable ScanT(3);
	ScanT.InsRecord("May Nancy", data2);
	ScanT.InsRecord("Underwood Joanna", data3);
	ScanT.InsRecord("Briggs Holly", data1);

	TSortTable SortT(3);
	SortT.SetSortMethod(QUICK_SORT);
	SortT = ScanT;

	EXPECT_EQ(SortT.GetKey(FIRST_POS), "Briggs Holly");
	EXPECT_EQ(SortT.GetKey(LAST_POS), "Underwood Joanna");
}



//TREE TABLE TEST


TEST(TTreeTableTest, can_create_tree_table)
{
	ASSERT_NO_THROW(TTreeTable TreeTab);
}

TEST(TTreeTableTest, can_reset)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("OS", 5));

	TreeTab.Reset();
	EXPECT_EQ(TreeTab.GetKey(), "Briggs Holly");
}

TEST(TTreeTableTest, can_go_next)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("OS", 5));

	TreeTab.Reset();
	TreeTab.GoNext();
	EXPECT_EQ(TreeTab.GetKey(), "May Nancy");
}

TEST(TTreeTableTest, can_delete_alone)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));

	TreeTab.DelRecord("Wilkerson Caitlin");
	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Underwood Joanna");
	ASSERT_ANY_THROW(TreeTab.FindRecord("Wilkerson Caitlin"));
}

TEST(TTreeTableTest, can_delete_only_left)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));

	TreeTab.DelRecord("Underwood Joanna");

	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Wilkerson Caitlin");
	ASSERT_ANY_THROW(TreeTab.FindRecord("Underwood Joanna"));
}

TEST(TTreeTableTest, can_delete_node_with_two_children_with_right_child)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Green Robert", new Students("AVS", 4));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Robbins Paul", new Students("AVS", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));


	TreeTab.DelRecord("Green Robert");

	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Robbins Paul");
	ASSERT_ANY_THROW(TreeTab.FindRecord("Green Robert"));
}

TEST(TTreeTableTest, cant_insert_record_with_already_existing_key)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Green Robert", new Students("AVS", 4));

	ASSERT_ANY_THROW(TreeTab.InsRecord("Briggs Holly", nullptr));
}

TEST(TTreeTableTest, can_insert_in_tree_table)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));

	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Underwood Joanna");
}

TEST(TTreeTableTest, can_insert_after_delete)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));

	TreeTab.DelRecord("Wilkerson Caitlin");
	TreeTab.DelRecord("Underwood Joanna");

	TreeTab.InsRecord("Rose Mary", new Students("Philosphy", 4));

	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Rose Mary");
}

TEST(TTreeTableTest, IsEmpty_return_true_if_tree_table_is_empty)
{
	TTreeTable TreeTab;

	EXPECT_TRUE(TreeTab.IsEmpty());
}

TEST(TTreeTableTest, tree_table_is_always_sorted)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", nullptr);

	TreeTab.Reset();

	EXPECT_EQ(TreeTab.GetKey(), "Briggs Holly");
	TreeTab.GoNext();
	EXPECT_EQ(TreeTab.GetKey(), "May Nancy");
	TreeTab.GoNext();
	EXPECT_EQ(TreeTab.GetKey(), "Underwood Joanna");
	TreeTab.GoNext();
	EXPECT_EQ(TreeTab.GetKey(), "Wilkerson Caitlin");
}

TEST(TTreeTableTest, cant_remove_record_with_nonexistent_key)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));

	ASSERT_ANY_THROW(TreeTab.DelRecord("Underwood Joanna"));
}

TEST(TTreeTableTest, cant_remove_record_from_empty_tree_table)
{
	TTreeTable TreeTab;

	ASSERT_ANY_THROW(TreeTab.DelRecord("Underwood Joanna"));
}

TEST(TTreeTableTest, removing_record_with_random_key_does_not_break_table_sorted_state)
{
	TTreeTable TreeTab;

	TreeTab.InsRecord("May Nancy", new Students("History", 4));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", nullptr);

	TreeTab.Reset();
	TreeTab.DelRecord("Underwood Joanna");

	EXPECT_EQ(TreeTab.GetKey(), "Briggs Holly");
	TreeTab.GoNext();
	EXPECT_EQ(TreeTab.GetKey(), "May Nancy");
	TreeTab.GoNext();
	EXPECT_EQ(TreeTab.GetKey(), "Wilkerson Caitlin");
}


//BALANCE TREE TABLE TEST

TEST(TBalanceTreeTest, can_create_balance_tree)
{
	ASSERT_NO_THROW(TBalanceTree bt);
}

TEST(TBalanceTreeTest, can_insert_record_into_balance_tree)
{
	TBalanceTree bt;

	ASSERT_NO_THROW(bt.InsRecord("May Nancy", new Students("History", 4)));
}

TEST(TBalanceTreeTest, cant_insert_record_in_balance_tree_with_already_existing_key)
{
	TBalanceTree bt;

	bt.InsRecord("Chase Willis", new Students("AVS", 4));
	bt.InsRecord("Briggs Holly", new Students("AVS", 5));
	bt.InsRecord("Green Robert", new Students("AVS", 4));

	ASSERT_ANY_THROW(bt.InsRecord("Briggs Holly", nullptr));
}

TEST(TBalanceTreeTest, can_insert_in_table_with_right_rotate)
{
	TBalanceTree TreeTab;


	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));

	TreeTab.Reset();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Chase Willis");
}

TEST(TBalanceTreeTest, proper_distribution_of_balances_when_insert)
{
	TBalanceTree TreeTab;
	int balRoot, balRotateNode;

	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Keyn Mary", new Students("AVS", 5));

	TreeTab.Reset();
	TreeTab.GoNext();
	balRoot = TreeTab.GetBalance();
	TreeTab.GoNext();
	TreeTab.GoNext();

	balRotateNode = TreeTab.GetBalance();

	EXPECT_EQ(balRoot, Right);
	EXPECT_EQ(balRotateNode, OK);
}

TEST(TBalanceTreeTest, can_delete_alone_with_left_balancing)
{
	TBalanceTree TreeTab;

	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));

	TreeTab.DelRecord("Wilkerson Caitlin");

	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Dixon Nancy");
	ASSERT_ANY_THROW(TreeTab.FindRecord("Wilkerson Caitlin"));
}


TEST(TBalanceTreeTest, can_delete_root)
{
	TBalanceTree TreeTab;

	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));

	TreeTab.DelRecord("Underwood Joanna");

	TreeTab.Reset();
	TreeTab.GoNext();
	TreeTab.GoNext();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Wilkerson Caitlin");
	ASSERT_ANY_THROW(TreeTab.FindRecord("Underwood Joanna"));
}


TEST(TBalanceTreeTest, can_delete_alone_with_right_balancing)
{
	TBalanceTree TreeTab;

	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));

	TreeTab.DelRecord("Briggs Holly");

	TreeTab.Reset();
	TreeTab.GoNext();

	EXPECT_EQ(TreeTab.GetKey(), "Dixon Nancy");
	ASSERT_ANY_THROW(TreeTab.FindRecord("Briggs Holly"));
}

TEST(TBalanceTreeTest, proper_distribution_of_balances_when_delete_alone_with_right_balancing)
{
	TBalanceTree TreeTab;
	int b1, b2;

	TreeTab.InsRecord("Chase Willis", new Students("AVS", 4));
	TreeTab.InsRecord("Dixon Nancy", new Students("History", 4));
	TreeTab.InsRecord("Briggs Holly", new Students("AVS", 5));
	TreeTab.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	TreeTab.InsRecord("Wilkerson Caitlin", new Students("AVS", 5));

	TreeTab.DelRecord("Briggs Holly");

	TreeTab.Reset();
	TreeTab.GoNext();
	b1 = TreeTab.GetBalance();
	TreeTab.GoNext();
	b2 = TreeTab.GetBalance();

	EXPECT_EQ(b1, Right);
	EXPECT_EQ(b2, Right);
}


TEST(TTreeTableTest, balance_tree_table_is_always_sorted)
{
	TBalanceTree bt;

	bt.InsRecord("May Nancy", new Students("History", 4));
	bt.InsRecord("Underwood Joanna", new Students("Philosphy", 5));
	bt.InsRecord("Briggs Holly", new Students("AVS", 5));
	bt.InsRecord("Wilkerson Caitlin", nullptr);

	bt.Reset();

	EXPECT_EQ(bt.GetKey(), "Briggs Holly");
	bt.GoNext();
	EXPECT_EQ(bt.GetKey(), "May Nancy");
	bt.GoNext();
	EXPECT_EQ(bt.GetKey(), "Underwood Joanna");
	bt.GoNext();
	EXPECT_EQ(bt.GetKey(), "Wilkerson Caitlin");
}

//ARRAY HASH TABLE TEST


TEST(TArrayHashTest, cant_insert_record_with_already_existing_key_into_array_hash_table)
{
	TArrayHash tah;

	tah.InsRecord("Chase Willis", new Students("AVS", 4));
	tah.InsRecord("Dixon Nancy", new Students("History", 4));
	tah.InsRecord("Briggs Holly", new Students("AVS", 5));

	ASSERT_ANY_THROW(tah.InsRecord("Chase Willis", new Students("AVS", 4)));
}

TEST(TArrayHashTest, cant_insert_record_into_full_array_hash_table)
{
	TArrayHash tah(5);

	tah.InsRecord("Chase Willis", new Students("AVS", 4));
	tah.InsRecord("Dixon Nancy", nullptr);
	tah.InsRecord("Briggs Holly", new Students("History", 4));
	tah.InsRecord("Underwood Joanna", nullptr);
	tah.InsRecord("Keyn Mary", nullptr);

	ASSERT_ANY_THROW(tah.InsRecord("Newman Carol", nullptr));
}

TEST(TArrayHashTest, can_find_record_in_array_hash_table)
{
	TArrayHash tah(5);

	tah.InsRecord("Chase Willis", new Students("AVS", 4));
	tah.InsRecord("Dixon Nancy", nullptr);
	tah.InsRecord("Briggs Holly", new Students("History", 4));

	ASSERT_NO_THROW(tah.FindRecord("Dixon Nancy"));
}

TEST(TArrayHashTest, cant_search_record_in_empty_array_hash_table)
{
	TArrayHash tah;

	ASSERT_ANY_THROW(tah.FindRecord("Keyn Mary"));
}

TEST(TArrayHashTest, cant_removing_record_with_nonexistent_key)
{
	TArrayHash tah;

	tah.InsRecord("Briggs Holly", new Students("History", 4));
	tah.InsRecord("Underwood Joanna", nullptr);
	tah.InsRecord("Keyn Mary", nullptr);

	ASSERT_ANY_THROW(tah.DelRecord("Newman Carol"));
}

TEST(TArrayHashTest, table_becomes_empty_after_removing_all_records)
{
	TArrayHash tah;

	tah.InsRecord("Briggs Holly", new Students("History", 4));
	tah.InsRecord("Underwood Joanna", nullptr);
	tah.InsRecord("Keyn Mary", nullptr);


	tah.DelRecord("Briggs Holly");
	tah.DelRecord("Underwood Joanna");
	tah.DelRecord("Keyn Mary");

	EXPECT_TRUE(tah.IsEmpty());
}

TEST(TArrayHashTest, can_insert_after_delete)
{
	TArrayHash tah(5);

	tah.InsRecord("Chase Willis", new Students("AVS", 4));
	tah.InsRecord("Dixon Nancy", nullptr);
	tah.InsRecord("Briggs Holly", new Students("History", 4));
	tah.InsRecord("Underwood Joanna", nullptr);
	tah.InsRecord("Keyn Mary", nullptr);

	tah.DelRecord("Briggs Holly");
	tah.DelRecord("Underwood Joanna");
	tah.DelRecord("Keyn Mary");

	tah.InsRecord("Briggs Holly", new Students("History", 4));
	tah.InsRecord("Underwood Joanna", nullptr);
	tah.InsRecord("Keyn Mary", nullptr);

	ASSERT_NO_THROW(tah.FindRecord("Chase Willis"));
	ASSERT_NO_THROW(tah.FindRecord("Dixon Nancy"));
	ASSERT_NO_THROW(tah.FindRecord("Briggs Holly"));
	ASSERT_NO_THROW(tah.FindRecord("Underwood Joanna"));
	ASSERT_NO_THROW(tah.FindRecord("Keyn Mary"));
}


//LIST HASH TABLE TEST



TEST(TListHashTest, cant_insert_record_with_already_existing_key)
{
	TListHash lht;

	lht.InsRecord("Briggs Holly", nullptr);

	ASSERT_THROW(lht.InsRecord("Briggs Holly", nullptr), std::runtime_error);
}

TEST(TListHashTest, can_find_record_in_the_list_hash_table)
{
	TListHash lht;

	lht.InsRecord("Briggs Holly", new Students("History", 4));
	lht.InsRecord("Underwood Joanna", nullptr);
	lht.InsRecord("Keyn Mary", nullptr);

	ASSERT_NO_THROW(lht.FindRecord("Keyn Mary"));
}

TEST(TListHashTest, cant_search_record_in_empty_list_hash_table)
{
	TListHash lht;

	ASSERT_ANY_THROW(lht.FindRecord("Keyn Mary"));
}


TEST(TListHashTest, cant_removing_record_from_empty_list_hash_table)
{
	TListHash lht;

	ASSERT_ANY_THROW(lht.DelRecord("Newman Carol"));
}

TEST(TListHashTest, cant_removing_record_with_nonexistent_key)
{
	TListHash lht;

	lht.InsRecord("Briggs Holly", nullptr);
	lht.InsRecord("Underwood Joanna", nullptr);
	lht.InsRecord("Keyn Mary", nullptr);

	ASSERT_ANY_THROW(lht.DelRecord("Newman Carol"));
}

TEST(TListHashTest, list_hash_table_becomes_empty_after_removing_all_records)
{
	TListHash lht;

	lht.InsRecord("Briggs Holly", nullptr);
	lht.InsRecord("Underwood Joanna", nullptr);
	lht.InsRecord("Keyn Mary", nullptr);

	lht.DelRecord("Briggs Holly");
	lht.DelRecord("Underwood Joanna");
	lht.DelRecord("Keyn Mary");

	EXPECT_TRUE(lht.IsEmpty());
}

TEST(TListHashTest, can_insert_after_delete_in_list_hash_table)
{
	TListHash lht;

	lht.InsRecord("Briggs Holly", nullptr);
	lht.InsRecord("Underwood Joanna", nullptr);
	lht.InsRecord("Keyn Mary", nullptr);

	lht.DelRecord("Briggs Holly");

	lht.InsRecord("Newman Carol", nullptr);

	ASSERT_NO_THROW(lht.FindRecord("Newman Carol"));
	ASSERT_NO_THROW(lht.FindRecord("Keyn Mary"));
	ASSERT_NO_THROW(lht.FindRecord("Underwood Joanna"));
	ASSERT_ANY_THROW(lht.FindRecord("Briggs Holly"));
}