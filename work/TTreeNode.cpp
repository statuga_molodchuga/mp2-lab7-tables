#include "TTreeNode.h"

PTTreeNode TTreeNode::GetLeft() const
{
	return (PTTreeNode)pLeft;
}

PTTreeNode TTreeNode::GetRight() const
{
	return (PTTreeNode)pRight;
}

TDatValue * TTreeNode::GetCopy()
{
	return new TTreeNode(Key, pValue->GetCopy(), pLeft, pRight);
}
