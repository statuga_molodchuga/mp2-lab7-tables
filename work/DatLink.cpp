#include "DatLink.h"

TDatLink::TDatLink(PTDatValue pVal, PTRootLink pN) : TRootLink(pN)
{
	pValue = pVal;
}

void TDatLink::SetDatValue(PTDatValue pVal)
{
	pValue = pVal;
}

PTDatValue TDatLink::GetDatValue()
{
	return pValue;
}

TDatLink::~TDatLink()
{
	if (pValue != nullptr) delete pValue;
	pValue = nullptr;
}

PTDatLink TDatLink::GetNextDatLink()
{
	return (PTDatLink)pNext;
}
