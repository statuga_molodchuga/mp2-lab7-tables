#include "TSortTable.h"

TSortTable::TSortTable(const TScanTable & tab) : TScanTable(tab)
{
	SetSortMethod(INSERT_SORT);
	SortData();
	Reset();
}

TSortTable & TSortTable::operator=(const TScanTable & tab)
{
	if (this != &tab) {
		if (pRecs != nullptr) {
			for (int i = 0; i < DataCount; i++) {
				if (pRecs[i] != nullptr)
					if (pRecs[i]->pValue != tab.pRecs[i]->pValue) {
						delete pRecs[i];
					}
			}
			delete[] pRecs;
		}

		int i = 0;
		CurrPos = 0;
		TabSize = tab.TabSize;
		DataCount = tab.DataCount;

		pRecs = new PTTabRecord[TabSize];
		for (; i < DataCount; ++i)
			pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();

		while (i < TabSize) pRecs[i++] = nullptr;
	}
	SortData();
	Reset();
	return *this;
}

PTDatValue TSortTable::FindRecord(TKey k)
{
	Reset();

	if (IsEmpty())
		throw std::runtime_error("Table is empty");

	if (k < pRecs[0]->GetKey() || k > pRecs[DataCount - 1]->GetKey())
	{
		if (k > pRecs[DataCount - 1]->GetKey())
			CurrPos = DataCount;
		throw std::runtime_error("There is no record with this key in the table");
	}

	Efficiency = 0;
	int left = 0, right = DataCount;
	while ((right - left) > 0)
	{
		Efficiency++;
		int pos = left + (right - left) / 2;
		SetCurrentPos(pos);

		if (pRecs[pos]->GetKey() == k)
			return pRecs[pos]->GetValuePTR();
		else if (k > pRecs[pos]->GetKey())
			left = pos + 1;
		else right = pos;
	}
	SetCurrentPos(right);

	throw std::runtime_error("There is no record with this key in the table");
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull())
		throw std::runtime_error("Table is full");

	try {
		FindRecord(k);
	}
	catch (...) {
		for (int i = DataCount; i > CurrPos; i--)
			pRecs[i] = pRecs[i - 1];
		pRecs[CurrPos] = new TTabRecord(k, pVal);
		DataCount++;
		return;
	}

	throw std::runtime_error("There is no record with this key in the table");
}

void TSortTable::DelRecord(TKey k)
{
	try {
		FindRecord(k);
	}
	catch (...) {
		throw std::runtime_error("There is no record with this key in the table");
	}

	delete pRecs[CurrPos];
	pRecs[CurrPos] = nullptr;

	for (int i = CurrPos; i < DataCount - 1; ++i)
		pRecs[i] = pRecs[i + 1];
	pRecs[DataCount - 1] = nullptr;
	DataCount--;
	Reset();
}

void TSortTable::SortData()
{
	Efficiency = 0;
	switch (SortMethod) {
	case INSERT_SORT: InsertSort(pRecs, DataCount); break;
	case MERGE_SORT:  MergeSort(pRecs, DataCount);  break;
	case QUICK_SORT:  QuickSort(pRecs, 0, DataCount - 1);  break;
	default:
		throw std::runtime_error("No sorting method is set"); break;
	}
}

void TSortTable::InsertSort(PTTabRecord * pMem, int DataCount)
{
	Efficiency = DataCount;
	PTTabRecord tmp_rec;
	for (int i = 0; i < DataCount; i++) {
		for (int j = i; j > 0 && pRecs[j]->GetKey() < pRecs[j - 1]->GetKey(); j--) {
			Efficiency++;
			tmp_rec = pRecs[j - 1];
			pRecs[j - 1] = pRecs[j];
			pRecs[j] = tmp_rec;
		}
	}
}

void TSortTable::MergeSort(PTTabRecord * pMem, int DataCount)
{
	PTTabRecord * pData = pMem;
	PTTabRecord * pBuff = new PTTabRecord[DataCount];
	PTTabRecord * pFirstBuff = pBuff;

	MergeSorter(pData, pBuff, DataCount);

	if (pData == pFirstBuff)
		for (int i = 0; i < DataCount; ++i) pBuff[i] = pData[i];
	delete pFirstBuff; // ???
}

void TSortTable::MergeSorter(PTTabRecord *& pData, PTTabRecord *& pBuff, int size)
{

	int n1 = (size + 1) / 2;
	int n2 = size - n1;
	if (size > 2)
	{
		PTTabRecord *pDat2 = pData + n1, *pBuff2 = pBuff + n1;
		MergeSorter(pData, pBuff, n1);
		MergeSorter(pDat2, pBuff2, n2);
	}
	MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *& pData, PTTabRecord *& pBuff, int size_left, int size_right)
{
	int i = 0, j = size_left, k = 0;
	while (i < size_left && j < size_left + size_right)
	{
		if (pData[i]->GetKey() < pData[j]->GetKey())
			pBuff[k++] = pData[i++];
		else
			pBuff[k++] = pData[j++];
	}
	while (i < size_left)
		pBuff[k++] = pData[i++];
	while (j < size_left + size_right)
		pBuff[k++] = pData[j++];

	PTTabRecord *tmp = new PTTabRecord[k];
	for (int i = 0; i < k; i++)
		tmp[i] = pData[i];
	for (int i = 0; i < k; i++)
		pData[i] = pBuff[i];
	for (int i = 0; i < k; i++)
		pBuff[i] = tmp[i];

	delete tmp;
}

void TSortTable::QuickSort(PTTabRecord *pMem, int left, int right)
{
	if (left < right)
	{
		int p = QuickSplit(pMem, left, right);
		QuickSort(pMem, left, p - 1);
		QuickSort(pMem, p, right);
	}
}

int TSortTable::QuickSplit(PTTabRecord *pData, int left, int right)
{
	PTTabRecord pivot = pData[(left + right) / 2];
	int i = left, j = right;
	while (i <= j)
	{
		while (pData[i]->GetKey() < pivot->GetKey()) i++;
		while (pData[j]->GetKey() > pivot->GetKey()) j--;

		if (i <= j)
		{
			PTTabRecord tmp = pData[i];
			pData[i] = pData[j];
			pData[j] = tmp;
			i++;
			j--;
		}
	}
	Efficiency = right;
	return i;
}