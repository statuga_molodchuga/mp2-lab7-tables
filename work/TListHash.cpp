#include "TListHash.h"

TListHash::TListHash(int size)
{
	if (size <= 0 || size > TabMaxSize)
		throw std::invalid_argument("Incorrect size");
	TabSize = size;
	pList = new PTDatList[TabSize];
	for (int i = 0; i < TabSize; ++i)
		pList[i] = new TDatList();
	CurrList = 0;
}

TListHash::~TListHash()
{
	if (pList != nullptr)
	{
		for (int i = 0; i < TabSize; ++i)
			if (pList[i] != nullptr)
				delete pList[i];
		delete[] pList; pList = nullptr;
	}
}

int TListHash::IsFull() const
{
	return false;
}

TKey TListHash::GetKey(void) const
{
	if ((CurrList <0) || IsTabEnded() || pList[CurrList]->IsListEnded())
		return std::string("");
	PTTabRecord pRec = PTTabRecord(pList[CurrList]->GetDatValue());
	return (pRec == nullptr) ? std::string("") : pRec->Key;
}

PTDatValue TListHash::GetValuePTR(void) const
{
	if ((CurrList <0) || IsTabEnded())
		return nullptr;
	PTTabRecord pRec = PTTabRecord(pList[CurrList]->GetDatValue());
	return(pRec == nullptr) ? nullptr : pRec->pValue;
}

PTDatValue TListHash::FindRecord(TKey k)
{
	if (IsEmpty())
		throw std::runtime_error("FindRecord: Tree is empty");

	Efficiency = 0;

	CurrList = HashFunc(k) % TabSize;
	for (pList[CurrList]->Reset(); !pList[CurrList]->IsListEnded(); pList[CurrList]->GoNext())
	{
		Efficiency++;
		PTTabRecord pRec = PTTabRecord(pList[CurrList]->GetDatValue());

		if (pRec->GetKey() == k) return (PTTabRecord)pRec->GetValuePTR();
	}

	throw std::runtime_error("There is no record with this key in the table");
}

void TListHash::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull())
		throw std::runtime_error("Table if full");

	try { FindRecord(k); }
	catch (...)
	{
		CurrList = HashFunc(k) % TabSize;
		pList[CurrList]->InsFirst(new TTabRecord(k, pVal));
		DataCount++;
		return;
	}

	throw std::runtime_error("Record with this key already exists");
}

void TListHash::DelRecord(TKey k)
{
	try { FindRecord(k); }
	catch (...) {
		throw std::runtime_error("Record with this key already exists");
	}
	pList[CurrList]->DelCurrent();
	DataCount--;
}
int TListHash::Reset(void)
{
	CurrList = 0;
	while (!IsTabEnded() && pList[CurrList]->IsEmpty())
		CurrList++;
	if (IsTabEnded()) return -1;
	else
	{
		pList[CurrList]->Reset();
		return CurrList;
	}
}

int TListHash::IsTabEnded(void) const
{
	return CurrList == TabSize;
}

int TListHash::GoNext(void)
{
	pList[CurrList]->GoNext();
	if (!(pList[CurrList]->IsListEnded()))
		return CurrList;
	while (++CurrList && !IsTabEnded())
	{
		if (!(pList[CurrList]->IsEmpty()))
			return pList[CurrList]->Reset();
	}
	return 1;
}