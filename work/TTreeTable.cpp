#include "TTreeTable.h"

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
	if (pNode == nullptr) return;

	if (pNode->pLeft != nullptr)
		DeleteTreeTab(pNode->pLeft);
	if (pNode->pRight != nullptr)
		DeleteTreeTab(pNode->pRight);

	delete pNode;
	pNode = nullptr;
}

PTDatValue TTreeTable::FindRecord(TKey k, PTTreeNode& pParent)
{
	ppRef = pParent = nullptr;

	if (IsEmpty())
		throw std::runtime_error("FindRecord: Tree is empty");

	Efficiency = 0;
	PTTreeNode tmp_current = pRoot;

	while (tmp_current != nullptr)
	{
		pParent = ppRef;
		ppRef = tmp_current;
		Efficiency++;
		if (tmp_current->GetKey() == k) return tmp_current->GetValuePTR();
		else if (tmp_current->GetKey() > k) tmp_current = tmp_current->GetLeft();
		else tmp_current = tmp_current->GetRight();
	}

	throw std::runtime_error("There is no record with this key in the table");
}

void TTreeTable::DelRecord(TKey k)
{
	// reset->delete->goNext will break everything
	PTTreeNode pParent;
	try {
		FindRecord(k, pParent);
	}
	catch (...) {
		throw std::runtime_error("DelRecord: There is no record with this key in the table");
	}

	PTTreeNode node = ppRef;
	if (node->pRight == nullptr && node->pLeft == nullptr) {

		if (node == pRoot)
		{
			pRoot = nullptr;
		}
		else {
			TKey s1 = node->GetKey();
			TKey s2 = pParent->GetKey();
			if (s1 < s2) pParent->pLeft = nullptr;
			if (s1 > s2) pParent->pRight = nullptr;
		}

		delete node; node = nullptr;
	}
	else if (node->pLeft == nullptr) {
		PTTreeNode tmp_right = node->pRight;
		std::swap(node->Key, node->pRight->Key);
		std::swap(node->pValue, node->pRight->pValue);
		node->pRight = node->pRight->pRight;

		delete tmp_right;
	}
	else if (node->pRight == nullptr) {
		PTTreeNode  tmp_left = node->pLeft;
		std::swap(node->Key, node->pLeft->Key);
		std::swap(node->pValue, node->pLeft->pValue);
		node->pLeft = node->pLeft->pLeft;

		delete tmp_left;
	}
	else {
		if (node->pRight->pLeft == nullptr) {
			PTTreeNode tmp_right = node->pRight;
			std::swap(node->Key, node->pRight->Key);
			std::swap(node->pValue, node->pRight->pValue);
			node->pRight = node->pRight->pRight;

			delete tmp_right;
		}
		else {
			PTTreeNode tmp = node->pRight;
			while (tmp->pLeft != nullptr) tmp = tmp->pLeft;

			TKey tmpKey = tmp->Key;
			PTDatValue tmpVal = tmp->pValue->GetCopy();
			DelRecord(tmpKey);

			if (node->pValue != nullptr) {
				delete node->pValue;
				node->pValue = nullptr;
			}

			node->Key = tmpKey;
			node->pValue = tmpVal;
		}
	}
	DataCount--;
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull())
		throw std::runtime_error("InsRecord: Tree is full");

	try {
		FindRecord(k);
	}
	catch (...) {
		if (ppRef == nullptr) pRoot = new TTreeNode(k, pVal);
		else if (ppRef->GetKey() > k) ppRef->pLeft = new TTreeNode(k, pVal);
		else ppRef->pRight = new TTreeNode(k, pVal);
		DataCount++;
		return;
	}
	throw std::runtime_error("There is no record with this key in the table");
}

PTDatValue TTreeTable::FindRecord(TKey k)
{
	ppRef = nullptr;

	if (IsEmpty())
		throw std::runtime_error("FindRecord: Tree is empty");

	Efficiency = 0;
	PTTreeNode tmp_current = pRoot;

	while (tmp_current != nullptr)
	{
		ppRef = tmp_current;
		Efficiency++;
		if (tmp_current->GetKey() == k) return tmp_current->GetValuePTR();
		else if (tmp_current->GetKey() > k) tmp_current = tmp_current->GetLeft();
		else tmp_current = tmp_current->GetRight();
	}

	throw std::runtime_error("There is no record with this key in the table");
}

TKey TTreeTable::GetKey(void) const
{
	return (pCurrent == nullptr) ? nullptr : pCurrent->Key;
}
PTDatValue TTreeTable::GetValuePTR(void) const
{
	return (pCurrent == nullptr) ? nullptr : pCurrent->pValue;
}

int TTreeTable::Reset(void)
{
	PTTreeNode pNode = pCurrent = pRoot;
	while (!St.empty()) St.pop();
	CurrPos = 0;
	while (pNode != nullptr)
	{
		St.push(pNode);
		pCurrent = pNode;
		pNode = pNode->GetLeft();
	}
	return IsTabEnded();
}

int TTreeTable::IsTabEnded() const
{
	return CurrPos >= DataCount;
}

int TTreeTable::GoNext(void)
{
	if (!(CurrPos >= DataCount) && (pCurrent != nullptr))
	{
		PTTreeNode pNode = pCurrent = pCurrent->GetRight();
		St.pop();
		while (pNode != nullptr)
		{
			St.push(pNode);
			pCurrent = pNode;
			pNode = pNode->GetLeft();
		}
		if ((pCurrent == nullptr) && !St.empty())
			pCurrent = St.top();
		CurrPos++;
	}
	return IsTabEnded();
}
