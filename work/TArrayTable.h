#pragma once
#include "TTable.h"

#define TabMaxSize 35

enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class TSortTable;

class  TArrayTable : public TTable {
protected:
	PTTabRecord *pRecs;
	int TabSize;	
	int CurrPos;
public:
	TArrayTable(int size = TabMaxSize);
	~TArrayTable();

	int GetTabSize() const { return TabSize; }
	virtual int IsFull() const { return DataCount == TabSize; }

	virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
	virtual TKey GetKey(TDataPos mode) const;
	virtual PTDatValue GetValuePTR() const { return GetValuePTR(CURRENT_POS); }
	virtual PTDatValue GetValuePTR(TDataPos mode) const;

	virtual PTDatValue FindRecord(TKey k) = 0;
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0;
	virtual void DelRecord(TKey k) = 0;

	virtual int Reset();
	virtual int GoNext();
	virtual int IsTabEnded() const;

	virtual int SetCurrentPos(int pos);
	int GetCurrentPos() const { return CurrPos; }

	friend TSortTable;
};