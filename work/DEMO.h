#pragma once
#include "Students.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "TTable.h"

void Read(TTable& group, const char* file_name)
{
	std::ifstream file(file_name);
	if (file.is_open()) {
		std::string courses;
		std::string MaS;

		std::vector<std::string> arr_courses;

		std::getline(file, courses);

		for (std::size_t i = 0; i < courses.length(); i++)
		{
			std::string tmp;

			while (isspace(courses[i]))  i++;
			while (isalnum(courses[i]))  tmp += courses[i++];
			arr_courses.push_back(tmp);
		}

		while (!file.eof())
		{
			std::string student;
			Students* data = new Students();
			std::size_t i = 0, j = 0;

			std::getline(file, MaS);

			while (isspace(MaS[i])) i++;

			while (isalpha(MaS[i]))  student += MaS[i++];
			while (isspace(MaS[i])) { student += ' '; i++; }
			while (isalpha(MaS[i]))  student += MaS[i++];

			while (i < MaS.length()) {
				while (isspace(MaS[i])) i++;
				data->AddCource(arr_courses[j++], atoi(&(MaS[i++])));
			}
			group.InsRecord(student, data);
		}
		file.close();
	}
	else 
		throw std::invalid_argument("File not found");
}

std::ostream& Print(TTable& group, std::ostream& str = std::cout)
{
	group.Reset();
	for (int i = 0; i < 20; i++) str << " ";
	((Students*)(group.GetValuePTR()))->PrintCourses(str);
	for (group.Reset(); !group.IsTabEnded(); group.GoNext())
	{
		str << group.GetKey();
		for (std::size_t i = group.GetKey().length(); i < 20; i++) str << " ";

		((Students*)(group.GetValuePTR()))->PrintStudents(str);
	}
	str << std::endl;
	return str;
}

int GetStudentCourseMark(TTable& group, std::string key, std::string course)
{
	try {
		Students *tmp = ((Students*)(group.FindRecord(key)));
		return tmp->GetMark(course);
	}
	catch (...) { return 0; }
}

double GetStudentAvgMark(TTable& group, std::string key)
{
	try {
		Students *tmp = ((Students*)(group.FindRecord(key)));
		return tmp->GetAvgMark();
	}
	catch (...) { return 0; }
}

double GetGroupCourseAvgMark(TTable& group, std::string course)
{
	double res = 0.0;

	for (group.Reset(); !group.IsTabEnded(); group.GoNext())
		res += ((Students*)(group.GetValuePTR()))->GetMark(course);

	return group.GetDataCount() == 0 ? 0.0 : res / group.GetDataCount();
}

double GetGroupAvgMark(TTable& group)
{
	double res = 0.0;
	for (group.Reset(); !group.IsTabEnded(); group.GoNext())
		res += ((Students*)(group.GetValuePTR()))->GetAvgMark();
	return group.GetDataCount() == 0 ? 0.0 : res / group.GetDataCount();
}

int GetNumberOfExcellentStudents(TTable& group)
{
	int res = 0;
	for (group.Reset(); !group.IsTabEnded(); group.GoNext())
		if (((Students*)(group.GetValuePTR()))->GetAvgMark() == 5)
			res++;
	return res;
}

double GetAvgMarkAllGroup(TTable** group, int count)
{
	double res = 0.0;
	for (int i = 0; i< count; i++)
		res += GetGroupAvgMark((TTable&)*group[i]);

	return count == 0 ? 0.0 : res / count;
}

double GetAvgMarkCoursesAllGroup(TTable** group, int count, std::string course)
{
	double res = 0.0;
	for (int i = 0; i< count; i++)
		res += GetGroupCourseAvgMark((TTable&)*group[i], course);

	return count == 0 ? 0.0 : res / count;
}

int GetNumberExcellentAllGroup(TTable** group, int count)
{
	int num = 0;
	for (int i = 0; i < count; i++)
		num += GetNumberOfExcellentStudents((TTable&)*group[i]);

	return num;
}