#include "TTabRecord.h"

TTabRecord::TTabRecord(TKey k, PTDatValue pVal) :Key(k), pValue(pVal) {}

TTabRecord::TTabRecord(const TTabRecord & r)
{
	if (r.pValue != nullptr)
		pValue = r.pValue->GetCopy();
	else pValue = nullptr;
	Key = r.Key;
}

TTabRecord::~TTabRecord()
{
	if (pValue != nullptr) {
		delete pValue;
		pValue = nullptr;
	}
}

TDatValue * TTabRecord::GetCopy()
{
	if (pValue != nullptr)
		return new TTabRecord(Key, pValue->GetCopy());
	else
		return new TTabRecord(Key);
}

TTabRecord & TTabRecord::operator=(TTabRecord & tr)
{
	if (this != &tr)
	{
		if (tr.pValue != nullptr)
			pValue = tr.pValue->GetCopy();
		else pValue = nullptr;
		Key = tr.Key;
	}
	return *this;
}

int TTabRecord::operator==(const TTabRecord & tr)
{
	return Key == tr.Key;
}

int TTabRecord::operator<(const TTabRecord & tr)
{
	return Key < tr.Key;
}

int TTabRecord::operator>(const TTabRecord & tr)
{
	return Key > tr.Key;
}