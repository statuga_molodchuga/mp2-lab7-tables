#include "TScanTable.h"

TScanTable::TScanTable(const TScanTable & t)
{
	int i = 0;
	CurrPos = 0;
	TabSize = t.TabSize;
	DataCount = t.DataCount;

	pRecs = new PTTabRecord[TabSize];
	for (; i < DataCount; ++i)
		pRecs[i] = (PTTabRecord)t.pRecs[i]->GetCopy();

	if (i < TabSize) pRecs[i++] = nullptr;
}

TScanTable & TScanTable::operator=(const TScanTable & t)
{
	if (this != &t) {
		if (pRecs != nullptr) {
			for (int i = 0; i < DataCount; i++) {
				if (pRecs[i] != nullptr)
					if (pRecs[i]->pValue != t.pRecs[i]->pValue) {
						delete pRecs[i];
					}
			}
			delete[] pRecs;
		}

		int i = 0;
		CurrPos = 0;
		TabSize = t.TabSize;
		DataCount = t.DataCount;

		pRecs = new PTTabRecord[TabSize];
		for (; i < DataCount; ++i)
			pRecs[i] = (PTTabRecord)t.pRecs[i]->GetCopy();

		while (i < TabSize) pRecs[i++] = nullptr;
	}
	return *this;
}

PTDatValue TScanTable::FindRecord(TKey k)
{
	Reset();
	int i = 0;
	for (; i < DataCount; ++i)
		if (pRecs[i]->GetKey() == k) break;

	Efficiency = i + 1;

	if (i < DataCount)
	{
		CurrPos = i;
		return pRecs[i]->pValue;
	}
	else
		throw std::runtime_error("FindRecord: There is no record with this key in the table");
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull())
		throw std::runtime_error("InsRecord: Table is full");

	int i = 0;
	for (; i < DataCount; ++i)
		if (pRecs[i]->GetKey() == k) break;
	CurrPos = i;

	if (i < DataCount)
		throw std::runtime_error("InsRecord: Record with this key already exists");

	pRecs[DataCount++] = new TTabRecord(k, pVal);
}

void TScanTable::DelRecord(TKey k)
{
	if (IsEmpty())
		throw std::runtime_error("DelRecord: Table is empty");

	int i = 0;
	for (; i < DataCount; ++i)
		if (pRecs[i]->GetKey() == k) break;
	if (i == DataCount)
		throw std::runtime_error("DelRecord: There is no record with this key in the table");

	delete pRecs[i];
	pRecs[i] = nullptr;
	pRecs[i] = pRecs[DataCount - 1];
	DataCount--;
}