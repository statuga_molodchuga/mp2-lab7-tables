#include "TArrayTable.h"

TArrayTable::TArrayTable(int size) : TTable()
{
	if (size <= 0 || size > TabMaxSize)
		throw std::out_of_range("Size out of range");

	pRecs = new PTTabRecord[size];
	for (int i = 0; i < size; ++i)
		pRecs[i] = nullptr;
	TabSize = size;
	CurrPos = 0;
}

TArrayTable::~TArrayTable()
{
	if (pRecs != nullptr) {
		for (int i = 0; i < DataCount; i++) {
			if (pRecs[i] != nullptr) {
				//��������, ���� ���� ������, ����������� �� ���� � �� �� ������� ������
				delete pRecs[i];
			}
		}
		delete[] pRecs;
	}
}

TKey TArrayTable::GetKey(TDataPos mode) const
{
	if (IsEmpty())
		throw std::runtime_error("Table is empty");

	int pos;
	switch (mode) {
	case FIRST_POS:   pos = 0; 		       break;
	case CURRENT_POS: pos = CurrPos;       break;
	case LAST_POS:    pos = DataCount - 1; break;
	default:
		throw std::invalid_argument("The wrong position");
	}
	return (pRecs[pos] == nullptr) ? TKey("") : pRecs[pos]->Key;
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) const
{
	if (IsEmpty())
		throw std::runtime_error("Table is empty");

	int pos;
	switch (mode) {
	case FIRST_POS:   pos = 0; 		       break;
	case CURRENT_POS: pos = CurrPos;       break;
	case LAST_POS:    pos = DataCount - 1; break;
	default:
		throw std::invalid_argument("The wrong position");
	}

	return (pRecs[pos] == nullptr) ? nullptr : pRecs[pos]->pValue;
}

int TArrayTable::Reset()
{
	return CurrPos = 0;
}

int TArrayTable::GoNext()
{
	if (!IsFull()) return ++CurrPos;
	else return 1;
}

int TArrayTable::IsTabEnded() const
{
	return CurrPos >=DataCount;
}

int TArrayTable::SetCurrentPos(int pos)
{
	if (pos < 0 || pos >= DataCount)
		throw std::invalid_argument("The wrong position");
	return CurrPos = pos;
}