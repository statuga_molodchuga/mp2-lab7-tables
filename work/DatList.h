#pragma once
#include "DatLink.h"

enum TLinkPos { FIRST, CURRENT, LAST };
class TDatList
{
protected:
	PTDatLink pFirst;    
	PTDatLink pLast;     
	PTDatLink pCurrLink; 
	PTDatLink pPrevLink; 
	PTDatLink pStop;     
	int CurrPos;         
	int ListLen;         

	void DelLink(PTDatLink pLink);
public:
	 TDatList();
	virtual ~TDatList();
	PTDatValue GetDatValue(TLinkPos mode = CURRENT)	const; 
	int  GetListLength()							const;
	int  GetCurrentPos()							const;       
	int  GoNext();
	void SetCurrentPos(int pos);

	virtual bool IsListEnded() const;
	virtual bool IsEmpty()	   const;
	virtual int Reset();
										 
	virtual void InsFirst  (PTDatValue pVal = nullptr); 
	virtual void InsLast   (PTDatValue pVal = nullptr);
	virtual void InsCurrent(PTDatValue pVal = nullptr);
													 
	virtual void DelFirst();     
	virtual void DelCurrent();     
	virtual void DelList();
};
typedef TDatList* PTDatList;