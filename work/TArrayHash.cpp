#include "TArrayHash.h"

TArrayHash::TArrayHash(int size, int step) : THashTable()
{
	if (size <= 0 || size > TabMaxSize)
		throw std::out_of_range("Size out of range");

	if (step >= size || step < 0)
		throw std::out_of_range("Step out of range");

	TabSize = size;
	HashStep = step;

	pRecs = new PTTabRecord[size];
	for (int i = 0; i < size; ++i)
		pRecs[i] = nullptr;
	CurrPos = FreePos = 0;

	pMark = new TTabRecord(std::string("deleted"), nullptr);
}

TArrayHash::~TArrayHash()
{
	if (pRecs != nullptr) {
		for (int i = 0; i < DataCount; i++) {
			if (pRecs[i] != nullptr) {
				//????????, ???? ???? ??????, ??????????? ?? ???? ? ?? ?? ??????? ??????
				delete pRecs[i];
			}
		}
		delete[] pRecs;
	}
	delete pMark;
}

int TArrayHash::IsFull() const
{
	return DataCount == TabSize;
}

PTDatValue TArrayHash::FindRecord(TKey k)
{
	if (IsEmpty())
		throw std::runtime_error("FindRecord: Tree is empty");

	FreePos = -1;
	Efficiency = 0;

	CurrPos = HashFunc(k) % TabSize;
	for (int i = 0; i < TabSize; i++)
	{
		Efficiency++;
		if (pRecs[CurrPos] == nullptr)
			break;
		else if (pRecs[CurrPos] == pMark) {
			if (FreePos == -1) FreePos = CurrPos;
		}
		else if (pRecs[CurrPos]->GetKey() == k)
			return pRecs[CurrPos]->GetValuePTR();

		CurrPos = GetNextPos(CurrPos);
	}

	throw std::runtime_error("There is no record with this key in the table");
}

void TArrayHash::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull())
		throw std::runtime_error("Table if full");

	FreePos = -1;
	Efficiency = 0;
	CurrPos = HashFunc(k) % TabSize;

	for (int i = 0; i < TabSize; i++)
	{
		if (pRecs[CurrPos] != nullptr && pRecs[CurrPos]->Key == k)
			throw std::runtime_error("InsRecord: Record with this key already exists");
		else if (pRecs[CurrPos] == pMark && FreePos < 0) FreePos = CurrPos;
		else if (pRecs[CurrPos] == nullptr && FreePos < 0)
		{
			pRecs[CurrPos] = new TTabRecord(k, pVal);
			DataCount++;
			break;
		}
		else if (pRecs[CurrPos] == nullptr && FreePos > -1)
		{
			pRecs[FreePos] = new TTabRecord(k, pVal);
			DataCount++;
			break;
		}
		CurrPos = GetNextPos(CurrPos);
	}

	if (FreePos > -1)
	{
		pRecs[FreePos] = new TTabRecord(k, pVal);
		DataCount++;
		return;
	}
}
void TArrayHash::DelRecord(TKey k)
{
	try {
		PTDatValue tmp = FindRecord(k);
	}
	catch (...) {
		throw std::runtime_error("DelRecord: There is no record with this key in the table");
	}

	delete pRecs[CurrPos];
	pRecs[CurrPos] = pMark;
	DataCount--;
}

int TArrayHash::GetNextPos(int pos)
{
	return (pos + HashStep) % TabSize;
}
int TArrayHash::Reset(void)
{
	CurrPos = 0;
	if (!IsTabEnded())
	{
		while (CurrPos < TabSize)
		{
			if ((pRecs[CurrPos] != nullptr) && (pRecs[CurrPos] != pMark))
				break;
			CurrPos++;
		}
	}
	return IsTabEnded();

}
int TArrayHash::IsTabEnded(void) const
{
	return CurrPos >= TabSize;
}
int TArrayHash::GoNext(void)
{
	if (!IsTabEnded())
	{
		while (++CurrPos <= TabSize)
			if ((pRecs[CurrPos] != nullptr) && (pRecs[CurrPos] != pMark))
				break;
	}
	return IsTabEnded();
}
TKey TArrayHash::GetKey(void) const
{
	return ((CurrPos < 0) || (CurrPos >= TabSize)) ? std::string("") : pRecs[CurrPos]->Key;
}
PTDatValue TArrayHash::GetValuePTR(void) const
{
	return ((CurrPos < 0) || (CurrPos >= TabSize)) ? nullptr : pRecs[CurrPos]->pValue;
}