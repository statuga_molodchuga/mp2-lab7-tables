#include "TBalanceTree.h"

int TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
	int HeighIndex = HeightOK;
	if (pNode == nullptr)
	{
		pNode = new TBalanceNode(k, pVal);
		HeighIndex = HeightInc;
		DataCount++;
	}
	else if (k < pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pLeft, k, pVal))
			HeighIndex = LeftBalanc(pNode);
	}
	else if (k > pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pRight, k, pVal))
			HeighIndex = RightBalanc(pNode);
	}
	else
		throw std::runtime_error("Record with specified key already exists");
	return HeighIndex;
}

int TBalanceTree::DelBalanceTree(PTBalanceNode &pNode, TKey k, PTBalanceNode& pParent)
{
	int HeighIndex = HeightOK;
	if (pNode == nullptr)
		throw std::runtime_error("There is no record with specified key");

	if (k < pNode->Key) {
		if (DelBalanceTree((PTBalanceNode&)pNode->pLeft, k, pNode) == HeightOK)
			HeighIndex = RightBalanc(pNode);
	}
	else if (k > pNode->Key) {
		if (DelBalanceTree((PTBalanceNode&)pNode->pRight, k, pNode) == HeightOK)
			HeighIndex = LeftBalanc(pNode);
	}
	else {
		if (pNode->pRight == nullptr && pNode->pLeft == nullptr) {
			if (pNode != pParent) {
				TKey s1 = pNode->GetKey();
				TKey s2 = pParent->GetKey();
				if (s1 < s2) pParent->pLeft = nullptr;
				if (s1 > s2) pParent->pRight = nullptr;
			}
			int HeighIndex = HeightOK;
			delete pNode; pNode = nullptr;
		}
		else if (pNode->pLeft == nullptr) {
			PTTreeNode tmp_right = pNode->pRight;
			std::swap(pNode->Key, pNode->pRight->Key);
			std::swap(pNode->pValue, pNode->pRight->pValue);
			pNode->pRight = pNode->pRight->pRight;

			int HeighIndex = HeightOK;
			delete tmp_right;
		}
		else if (pNode->pRight == nullptr) {
			PTTreeNode  tmp_left = pNode->pLeft;
			std::swap(pNode->Key, pNode->pLeft->Key);
			std::swap(pNode->pValue, pNode->pLeft->pValue);
			pNode->pLeft = pNode->pLeft->pLeft;

			int HeighIndex = HeightOK;
			delete tmp_left;
		}
		else {
			PTTreeNode tmp = pNode->pRight;
			PTTreeNode tmp_parent = pNode;
			PTTreeNode tmp_node = pNode;

			while (tmp->pLeft != nullptr)
			{
				tmp_parent = tmp;
				tmp = tmp->pLeft;
			}

			TKey tmpKey = tmp->Key;
			PTDatValue tmpVal = tmp->pValue->GetCopy();

			if (!DelBalanceTree((PTBalanceNode&)tmp, tmpKey, (PTBalanceNode&)tmp_parent))
				HeighIndex = LeftBalanc(pNode);
			//datacount++;
			if (tmp_node->pValue != nullptr) {
				delete tmp_node->pValue;
				tmp_node->pValue = nullptr;
			}

			tmp_node->Key = tmpKey;
			tmp_node->pValue = tmpVal;

		}
	}
	DataCount--;
	return HeighIndex;
}

int TBalanceTree::LeftBalanc(PTBalanceNode &pNode)
{
	int HeighIndex = HeightOK;
	switch (pNode->GetBalance())
	{
	case Right: pNode->SetBalance(OK);   HeighIndex = HeightOK; break;
	case OK:    pNode->SetBalance(Left); HeighIndex = HeightInc;  break;
	case Left:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->pLeft);
		if (p1->GetBalance() == Left)
		{
			pNode->pLeft = p1->pRight;
			p1->pRight = pNode;
			pNode->SetBalance(OK);
			pNode = p1;
			pNode->SetBalance(OK);
		}
		else
		{
			p2 = PTBalanceNode(p1->pRight);
			p1->pRight = p2->pLeft;
			p2->pLeft = p1;
			pNode->pLeft = p2->pRight;
			p2->pRight = pNode;

			switch (p2->GetBalance())
			{
			case OK:
				pNode->SetBalance(OK);
				if (p1->GetBalance() == OK)
				{
					p1->SetBalance(Left);
					p2->SetBalance(Left);
				}
				else
				{
					p1->SetBalance(OK);
					p2->SetBalance(OK);
				}
				break;
			case Left:
				pNode->SetBalance(Right);
				p1->SetBalance(OK);
				p2->SetBalance(OK);
				break;
			case Right:
				p1->SetBalance(Left);
				pNode->SetBalance(OK);
				p2->SetBalance(OK);
				break;
			}
			pNode = p2;
		}
		HeighIndex = HeightOK;
	}
	return HeighIndex;
}

int TBalanceTree::RightBalanc(PTBalanceNode &pNode)
{
	int HeighIndex = HeightOK;
	switch (pNode->GetBalance())
	{
	case Left: pNode->SetBalance(OK); HeighIndex = HeightOK; break;
	case OK: pNode->SetBalance(Right); HeighIndex = HeightInc; break;
	case Right:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->pRight);
		if (p1->GetBalance() == Right)
		{
			pNode->pRight = p1->pLeft;
			p1->pLeft = pNode;
			pNode->SetBalance(OK);
			pNode = p1;
			pNode->SetBalance(OK);
		}
		else
		{
			p2 = PTBalanceNode(p1->pLeft);
			p1->pLeft = p2->pRight;
			p2->pRight = p1;
			pNode->pRight = p2->pLeft;
			p2->pLeft = pNode;
			switch (p2->GetBalance())
			{
			case OK:
				pNode->SetBalance(OK);
				if (p1->GetBalance() == OK)
				{
					p1->SetBalance(Right);
					p2->SetBalance(Right);
				}
				else
				{
					p1->SetBalance(OK);
					p2->SetBalance(OK);
				}
				break;
			case Left:
				p1->SetBalance(Right);
				pNode->SetBalance(OK);
				p2->SetBalance(OK);
				break;
			case Right:
				pNode->SetBalance(Left);
				p1->SetBalance(OK);
				p2->SetBalance(OK);
				break;
			}
			pNode = p2;
		}
		HeighIndex = HeightOK;
	}
	return HeighIndex;
}

void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull())
		throw std::runtime_error("Table is full");

	InsBalanceTree((PTBalanceNode&)(pRoot), k, pVal);
}

void TBalanceTree::DelRecord(TKey k)
{
	if (IsEmpty())
		throw std::runtime_error("Table is empty");

	DelBalanceTree((PTBalanceNode&)(pRoot), k, (PTBalanceNode&)pRoot);
}