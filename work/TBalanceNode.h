#pragma once
#include<iostream>
#include "TTreeNode.h"

#define OK 0
#define Left -1
#define Right 1 

class  TBalanceNode : public TTreeNode {
protected:
	int balance_factor;
public:
	TBalanceNode(TKey k = "", PTDatValue pVal = nullptr, PTTreeNode pL = nullptr, PTTreeNode pR = nullptr, int bal = OK) : TTreeNode(k, pVal, pL, pR),
		balance_factor(bal) {};
	virtual TDatValue * GetCopy();
	int  GetBalance() const { return balance_factor; }
	void SetBalance(int bal);
protected:

	friend class TBalanceTree;
};
typedef TBalanceNode *PTBalanceNode;
