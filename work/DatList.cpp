#include "DatList.h"
#include <stdexcept>

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr) delete pLink;
}

TDatList::TDatList()
{
	pStop = nullptr;
	pFirst = pLast = pCurrLink = pPrevLink = pStop;
	ListLen = 0;
	Reset();
}

TDatList::~TDatList()
{
	DelList();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	switch (mode)
	{
	case FIRST:
		return pFirst->pValue;
		break;
	case LAST:
		return pLast->pValue;
		break;
	default:
		return pCurrLink->pValue;
	};
}

int TDatList::GetListLength() const
{
	return ListLen;
}

void TDatList::SetCurrentPos(int pos)
{
	if (pos < 0 || pos >= ListLen)
		throw std::out_of_range("Incorrect position");
	Reset(); 
	for (int i = 0; i < pos; i++, GoNext());
}

int TDatList::GetCurrentPos() const
{
	return CurrPos;
}

int TDatList::Reset()
{
	if (!IsEmpty())
	{
		if (CurrPos != 0)
		{
			pPrevLink = pStop;
			pCurrLink = pFirst;
			CurrPos = 0;
		}
		return 1;
	}
	else
	{
		pCurrLink = pStop;
		pPrevLink = pStop;
		CurrPos = -1;
		return -1;
	}
}

bool TDatList::IsEmpty() const
{
	return pFirst == pStop;
}

bool TDatList::IsListEnded() const
{
	return pCurrLink == pStop;
}

int TDatList::GoNext()
{
	if (pCurrLink == pStop) return 1;
	pPrevLink = pCurrLink;
	pCurrLink = pCurrLink->GetNextDatLink();
	CurrPos++;
	return 0;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink tmp = new TDatLink(pVal, pFirst);
	pFirst = tmp; ListLen++;
	if (pLast == pStop) pLast = pCurrLink = tmp;
	if (CurrPos == 0)
	{
		pPrevLink = tmp;
		CurrPos = 1;
	}
	else
		CurrPos ++;
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink tmp = new TDatLink(pVal, pStop);
	if (IsEmpty())
	{
		pLast = pFirst = pCurrLink = tmp;
		Reset();
	}
	else
	{
		pLast->SetNextLink(tmp);
		pLast = tmp;
	}
	ListLen++;
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (IsEmpty() || pCurrLink == pFirst) InsFirst(pVal);
	else if (IsListEnded() || pCurrLink == pLast) InsLast(pVal);
	else
	{
		PTDatLink tmp = new TDatLink(pVal, pCurrLink);
		pPrevLink->SetNextLink(tmp);
		pPrevLink = tmp;
		ListLen++; SetCurrentPos(CurrPos + 1); 
	}
}

void TDatList::DelFirst()
{
	if(!IsEmpty())
	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		delete tmp;	ListLen--;

		if (IsEmpty())
			pPrevLink = pCurrLink = pFirst = pLast = pStop;
		if (CurrPos == 0) pCurrLink = pFirst;
		if (CurrPos == 1) pPrevLink = pStop;
		if (CurrPos > 0) CurrPos--;
	}
}

void TDatList::DelCurrent()
{
	if (!IsEmpty())
	{
		if (pCurrLink == pFirst) DelFirst();
		if (pCurrLink == pLast)
		{
			PTDatLink tmp = pCurrLink;
			pLast = pPrevLink;
			pCurrLink = pStop;
			ListLen--; DelLink(tmp);
		}
		else
		{
			PTDatLink tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			ListLen--; DelLink(tmp);
		}
	}
}

void TDatList::DelList()
{
	while (!IsEmpty()) DelFirst();
	pLast = pFirst = pCurrLink = pPrevLink = pStop;
	ListLen = 0; CurrPos = -1;
}