#pragma once
#include "TTreeTable.h"
#include "TBalanceNode.h"

// Has the height been changed
#define HeightOK 0
#define HeightInc 1

class  TBalanceTree : public TTreeTable {
protected:
	int InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	int DelBalanceTree(PTBalanceNode &pNode, TKey k, PTBalanceNode& pParent);

	int LeftBalanc(PTBalanceNode &pNode);
	int RightBalanc(PTBalanceNode &pNode);
public:
	TBalanceTree() :TTreeTable() {}

	int  GetBalance() const { return ((PTBalanceNode)pCurrent)->balance_factor; }
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual void DelRecord(TKey k);
};