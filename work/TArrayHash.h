#pragma once

#define TabMaxSize 35   // Guarantee of finding
#define TabHashStep 3   // free lines in the table

#include "TTabRecord.h"
#include "THashTable.h"

class  TArrayHash : public THashTable
{
protected:
	PTTabRecord *pRecs;          // память для записей таблицы
	int TabSize;                 // макс. возм. к-во записей
	int HashStep;                // шаг вторичного перемешивания
	int FreePos;                 // первая свободная строка, обнаруженная при поиске
	int CurrPos;                 // строка памяти при завершении поиска
	PTTabRecord pMark;           // маркер для индикации строк с удаленными записями
	int GetNextPos(int pos);     // откр. перем.

public:
	TArrayHash(int size = TabMaxSize, int step = TabHashStep);
	~TArrayHash();

	virtual int IsFull() const; 

	virtual TKey GetKey(void) const;
	virtual PTDatValue GetValuePTR(void) const;

	virtual PTDatValue FindRecord(TKey k);          
	virtual void InsRecord(TKey k, PTDatValue pVal); 
	virtual void DelRecord(TKey k);                   

												
	virtual int Reset(void);                   
	virtual int IsTabEnded() const;       
	virtual int GoNext(void);
};