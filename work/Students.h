#pragma once
#include "TDatValue.h"
#include <iostream>
#include <string>
class Students : public TDatValue
{
private:
	struct Course
	{
		std::string name;
		int mark;

		Course() : name(" "), mark(-1) {}
		Course(std::string n, int m) : name(n), mark(m) {}
		~Course() {}
	};

	std::size_t size;
	Course* cource;
public:
	Students() : size(0), cource(nullptr) {}
	Students(const Students& m);
	Students(std::string name, int mark);
	~Students();

	void AddCource(std::string name, int mark);
	double GetAvgMark() const;
	int  GetMark(std::string CourseName) const;
	void SetMark(std::string CourseName, int mark);
	size_t GetSize() const { return size; }

	bool operator==(const Students& m) const;
	bool operator!=(const Students& m) const;

	void PrintCourses(std::ostream& s);
	void PrintStudents(std::ostream& s);

	virtual TDatValue * GetCopy();
};

