#pragma once
#include "TDatValue.h"
#include <string>

typedef std::string TKey;

class TTabRecord : public TDatValue
{
protected:
	TKey Key;
	PTDatValue pValue;
public:
	TTabRecord(TKey k = "", PTDatValue pVal = nullptr);
	TTabRecord(const TTabRecord & r);
	~TTabRecord();

	TKey GetKey(void) { return Key; }
	void SetKey(TKey k) { Key = k; }
	PTDatValue GetValuePTR(void) { return pValue; }
	void SetValuePtr(PTDatValue p) { pValue = p; }
	TTabRecord & operator = (TTabRecord &tr);
	virtual TDatValue * GetCopy();

	virtual int operator == (const TTabRecord &tr);
	virtual int operator <  (const TTabRecord &tr);
	virtual int operator >  (const TTabRecord &tr);

	friend class TArrayTable;
	friend class TScanTable;
	friend class TSortTable;
	friend class TTreeNode;
	friend class TTreeTable;
	friend class TArrayHash;
	friend class TListHash;
};
typedef TTabRecord* PTTabRecord;
