#pragma once
#include "TArrayTable.h"

class TScanTable : public TArrayTable
{
public:
	TScanTable(int size = TabMaxSize) : TArrayTable(size) {}
	TScanTable(const TScanTable& t);
	TScanTable& operator=(const TScanTable& t);

	virtual PTDatValue FindRecord(TKey k);
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual void DelRecord(TKey k);
};