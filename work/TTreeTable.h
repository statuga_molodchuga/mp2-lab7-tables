#pragma once
#include "TTable.h"
#include "TTreeNode.h"
#include <stack>

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;// ��������� �� ������ ������
	PTTreeNode ppRef;// ����� �������� �� �������-���������� � FindRecord
	PTTreeNode pCurrent;// ��������� �� ������� �������
	std::stack < PTTreeNode> St; // ���� ��� ���������
	int CurrPos;// ����� ������� �������

	void DeleteTreeTab(PTTreeNode pNode);
	PTDatValue  FindRecord(TKey k, PTTreeNode& pParent);
public:
	TTreeTable() : TTable(), pRoot(nullptr), ppRef(nullptr), pCurrent(nullptr), CurrPos(0) {}
	~TTreeTable() { DeleteTreeTab(pRoot); }

	virtual int IsFull() const { return false; }
	virtual void DelRecord(TKey k);
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual PTDatValue FindRecord(TKey k);

	virtual TKey GetKey(void) const;
	virtual PTDatValue GetValuePTR(void) const;
	virtual int Reset(void);
	virtual int GoNext(void);
	virtual int IsTabEnded() const;
};