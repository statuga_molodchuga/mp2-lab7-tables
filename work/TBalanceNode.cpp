#include "TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
	return new TBalanceNode(Key, pValue, pLeft, pRight, OK);
}

void TBalanceNode::SetBalance(int bal)
{
	if (bal > 1 || bal < -1)
		throw std::invalid_argument("Incorrect balance!");
	else balance_factor = bal;
}
