#pragma once
#include "RootLink.h"
class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink
{
protected:
	PTDatValue pValue;  
public:
	TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr);
	PTDatLink  GetNextDatLink();

	virtual void SetDatValue(PTDatValue pVal);
	virtual PTDatValue GetDatValue();

	virtual ~TDatLink();
	friend class TDatList;
};