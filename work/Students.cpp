#include "Students.h"

Students::Students(const Students & m)
{
	size = m.size;
	if (size == 0)
		cource = nullptr;
	else
	{
		cource = new Course[size];

		for (std::size_t i = 0; i < size; ++i)
			cource[i] = m.cource[i];
	}
}

Students::Students(std::string name, int mark)
{
	AddCource(name, mark);
}

Students::~Students()
{
	delete[] cource;
	cource = nullptr;
	size = 0;
}

void Students::AddCource(std::string name, int mark)
{
	if (size == 0)
	{
		cource = new Course[1];
		cource[0].mark = mark;
		cource[0].name = name;
	}
	else
	{
		auto tmp = cource;
		cource = new Course[size + 1];

		for (std::size_t i = 0; i < size; ++i)
			cource[i] = tmp[i];

		cource[size].name = name;
		cource[size].mark = mark;

		delete[] tmp;
	}
	size++;
}

double Students::GetAvgMark() const
{
	double res = 0.0;

	for (std::size_t i = 0; i < size; ++i)
		res += (double)cource[i].mark;

	return size == 0 ? 0.0 : res / size;
}

int Students::GetMark(std::string CourseName) const
{
	for (std::size_t i = 0; i < size; ++i)
		if (cource[i].name == CourseName) return cource[i].mark;

	return 0;
}

void Students::SetMark(std::string CourseName, int mark)
{
	for (std::size_t i = 0; i < size; ++i)
		if (cource[i].name == CourseName) cource[i].mark = mark;
}

bool Students::operator==(const Students & m) const
{
	if (size != m.size) return false;
	std::size_t res = 0;
	for (std::size_t i = 0; i < size; i++)
	{
		for (std::size_t j = 0; j < size; j++)
		{
			if (cource[i].name == m.cource[j].name && cource[i].mark == m.cource[j].mark)
				res++;
		}
	}
	return res == size;
}

bool Students::operator!=(const Students & m) const
{
	if (size != m.size) return true;
	std::size_t res = 0;
	for (std::size_t i = 0; i < size; i++)
	{
		for (std::size_t j = 0; j < size; j++)
		{
			if (cource[i].name == m.cource[j].name && cource[i].mark == m.cource[j].mark)
				res++;
		}
	}
	return res != size;
}

void Students::PrintCourses(std::ostream & s)
{
	for (std::size_t i = 0; i < size; i++)
		s << cource[i].name << " ";
	s << std::endl;
}

void Students::PrintStudents(std::ostream & s)
{
	for (std::size_t i = 0; i < size; i++) {
		for (std::size_t j = 0; j < cource[i].name.length() / 2; j++)
			s << " ";
		s << cource[i].mark;
		for (std::size_t j = 0; j < cource[i].name.length() / 2; j++)
			s << " ";
	}
	s << std::endl;
}

TDatValue * Students::GetCopy()
{
	return (new Students(*this));
}