#pragma once
#include "TScanTable.h"

enum TSortMethod { INSERT_SORT, MERGE_SORT, QUICK_SORT };

class  TSortTable : public TScanTable {
protected:
	TSortMethod SortMethod;
	void SortData();

	void InsertSort(PTTabRecord *pMem, int DataCount);
	void MergeSort(PTTabRecord *pMem, int DataCount);
	void QuickSort(PTTabRecord *pMem, int left, int right);

	void MergeSorter(PTTabRecord *&pData, PTTabRecord *&pBuff, int size);
	void MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int size_left, int size_right);
	int  QuickSplit(PTTabRecord *pData, int left, int right);

public:
	TSortTable(int size = TabMaxSize) : TScanTable(size) {};
	TSortTable(const TScanTable &tab);
	TSortTable & operator=(const TScanTable &tab);

	TSortMethod GetSortMethod() { return SortMethod; }
	void SetSortMethod(TSortMethod sm) { SortMethod = sm; }

	virtual PTDatValue FindRecord(TKey k);
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual void DelRecord(TKey k);
};